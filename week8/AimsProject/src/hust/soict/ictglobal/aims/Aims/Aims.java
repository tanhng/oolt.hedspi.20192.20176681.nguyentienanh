package hust.soict.ictglobal.aims.Aims;
//import hust.soict.ictglobal.aims.disc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;
import java.util.*;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.*;
public class Aims {
	public static void showMenu()
	{
		System.out.println("Order Management Application");
		System.out.println("-------------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("5. Test");
		System.out.println("0. Exit");
		System.out.println("-------------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		Order order = new Order();
		
		do
		{
			Aims.showMenu();
			try {
				choice = Integer.parseInt(keyboard.nextLine());
			} catch(NumberFormatException e)
			{
				e.printStackTrace();
			}
			
		
			switch(choice)
			{
				case 1:
				{
					if(Order.nOrdered < Order.max_OrderNumber)
					{
						System.out.println("/n/n");
						order = new Order();
						System.out.println("added success");
					}
					else
					{
						System.out.println("reached max orders number");
					}
					
					System.out.println();
					break;
				}
				case 2:
{	
					System.out.println("/n/n");
					System.out.println();
					System.out.println("Book, DigitalVideoDisc or CompactDisc? ");
					String type = keyboard.nextLine();
					System.out.print("Enter title: ");
					String title = keyboard.nextLine();
					System.out.print("Enter category: ");
					String category = keyboard.nextLine();
					System.out.print("Enter cost: ");
					float cost = Float.parseFloat(keyboard.nextLine());
					if(type.equals("Book"))
					{
						Book item = new Book(title, category);
						item.setCost(cost);
						order.addMedia(item);
					}
					else if(type.equals("DigitalVideoDisc"))
					{
						DigitalVideoDisc item = new DigitalVideoDisc(title);
						item.setCategory(category);
						item.setCost(cost);
						order.addMedia(item);
					}
					else if(type.equals("CompactDisc"))
					{
						CompactDisc item = new CompactDisc(title);
						item.setCategory(category);
						item.setCost(cost);
						order.addMedia(item);
					}
					else {
						System.out.println("type invalid");
					}
					break;
				}
				case 3:
				{
					System.out.println("/n/n");
					System.out.println();
					System.out.print("Enter index: ");
					int index = Integer.parseInt(keyboard.nextLine());
					order.removeMedia(index);
					break;
				}
				case 4:
				{
					System.out.println("/n/n");
					for(int i = 0; i < order.getQtyOrdered(); i++)
					{
						order.getItemsOrdered().get(i).print();
					}
					
					break;
				}
				case 5:
				{
					java.util.Collection<DigitalVideoDisc> collection = new java.util.ArrayList<DigitalVideoDisc>();
					Scanner sc = new Scanner(System.in);
					int exit = 1;
					
					do {
						System.out.print("Enter title: ");
						String title = sc.nextLine();
						System.out.print("Enter the lenght of dvd: ");
						int length = Integer.parseInt(sc.nextLine());
						System.out.print("Enter cost: ");
						float cost = Float.parseFloat(sc.nextLine());
						
						System.out.println("\n-------------------------------------------");
						DigitalVideoDisc dvd = new DigitalVideoDisc(title);
						dvd.setCost(cost);
						dvd.setLength(length);
						collection.add(dvd);
						System.out.print("Enter 0 to exit or 1 to continue: ");
						exit = Integer.parseInt(sc.nextLine());
					}while(exit != 0);
					
					// iterate through the array list and output their titles
					java.util.Iterator<DigitalVideoDisc> iterator = collection.iterator();
					
					System.out.println("\n-----------------------------------------");
					System.out.println("The DVDs currently in the order are: ");
					
					while(iterator.hasNext())
					{
						System.out.println(((Media)iterator.next()).getTitle());
					}
					
					// Sort the collection of DVDs - based on the compareTo()
					java.util.Collections.sort((java.util.List<DigitalVideoDisc>)collection);
					
					iterator = collection.iterator();
					System.out.println("\n-----------------------------------------");
					System.out.println("The DVDs in sorted order are: ");
					
					while(iterator.hasNext())
					{
						System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
					}
					
					System.out.println("-----------------------------------------");
				}
				case 0: System.out.println("exit"); break;
				default: System.out.println("xxx"); break;
			}
		}while(choice != 0);
		keyboard.close();
	}

}

